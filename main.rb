require_relative 'Student.rb'
require_relative 'Teacher.rb'

name = "Thauan"
age = "21"
cpf = "15582492605"
born = "23/03/1998"
adress = "Rua Aquidabã,280"
day_week = "segunda"

person = Person.new name,age,cpf,born,adress
person.humor day_week
person.activity
person.apresentation do |name,age,cpf,born,adress,humor,activity| 
    puts "Nome: #{name}"
    puts "Idade: #{age}"
    puts "CPF: #{cpf}"
    puts "Nascido em: #{born}"
    puts "Endereco: #{adress}"
    puts "Humor: #{humor}"
    puts "Fazendo: #{activity}"
    puts
end

name = "Robsvaldo"
age = "35"
cpf = "821475293300"
born = "30/02/2000"
adress = "Ponte Rio Niteroi"
matricula = "217031138"

student = Student.new matricula,name,age,cpf,adress,born
student.activity

student.apresentation do |name,age,cpf,adress,born,activity|
    puts "Nome: #{student.name}"
    puts "CPF: #{student.cpf}"
    puts "Idade: #{student.age}"
    puts "Nascimento: #{student.born}"
    puts "Endereço: #{adress}"
    puts "Matricula: #{matricula}"
    puts "Esta Estudando: #{student.activity}"
    puts
end

name = "Marina"
age = "33"
cpf ="123456789"
born = "28/10/1985"
adress = "Canada"
materias = ["ED","PE","Calculo"]
formacao = ["Doutorado Em CC","Mestrado na Google"]

teacher = Teacher.new name,age,cpf,born,adress,materias,formacao

teacher.apresentation do |name,age,cpf,adress,born|
    puts "Nome: #{teacher.name}"
    puts "Idade: #{teacher.age}"
    puts "CPF: #{teacher.cpf}"
    puts "Nascimento: #{teacher.born}"
    puts "Endereco: #{adress}"
    puts "Materias: #{materias}"
    puts "Formacao: #{formacao}"
end