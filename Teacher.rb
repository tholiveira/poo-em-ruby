require_relative 'Person.rb'

class Teacher < Person
    
    def initialize name,age,cpf,born,adress,materias,formacao
        super name,age,cpf,adress,born
        @materias = materias
        @formacao = formacao
    end

    def addmaterias materia 
        @materias << materia
    end

    def apresentation
        super
    end
end