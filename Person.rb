class Person
    attr_accessor :name
    attr_accessor :age
    attr_accessor :cpf
    attr_accessor :address
    attr_accessor :born

    @activity
    @humor

    def initialize name,age,cpf,adress,born
      @name = name
      @age = age
      @cpf = cpf
      @adress = adress
      @born = born
    end

    def apresentation
        yield @name,@age,@cpf,@adress,@born,@humor,@activity
    end

    def humor day_week
        humor_hash = {segunda:"feliz",terca:"triste",quarta:"chateadao",quinta:"bravo",sexta:"eletrico",sabado:"animado",domingo:"radiante"}    
        symh = day_week.to_sym
        @humor = humor_hash[day_week.to_sym]
    end

    def activity
        @activity = "codando"
    end
end