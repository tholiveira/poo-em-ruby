require_relative 'Person.rb'

class Student < Person
    
    def initialize mat,name,age,cpf,adress,born
        @mat = mat
        super name,age,cpf,adress,born
    end

    def locomotion? 
        false
    end

    def studying?
       if locomotion? == true 
            studiyng = true
       else
            studiyng = false
       end
    end

    def apresentation
        super
    end

    def activity
        if studying?
            @activity = "Estudando"
        else
            @activity = "Indo para a Escola"
        end
    end
end